#!/usr/bin/python3

import sys, os, json, argparse
from typing import Any, Dict, List, Tuple
import youtube_dl

INFO = "INFO: "
WARN = "WARN: "
ERROR = "ERROR: "

working_dir = os.path.dirname(os.path.realpath(__file__))

parser = argparse.ArgumentParser()
subparser = parser.add_subparsers(dest="command")

add = subparser.add_parser("add")
add.add_argument("url")

remove = subparser.add_parser("remove")
remove.add_argument("url")

sync = subparser.add_parser("sync")
sync.add_argument("url")

args = parser.parse_args()

archives = {}
try:
    with open(f"{working_dir}/archives.json") as f:
        archives = json.load(f)
except Exception:
    print(f"{WARN}Could not find archives file.")
    archives.update({"archives": []})


if args.command == "add":
    archive = {
        "url": args.url,
    }

    new_archive = True
    for old_archive in archives["archives"]:
        if old_archive["url"] == args.url:
            new_archive = False

    if new_archive:
        archives["archives"].append(archive)
        print(f"{INFO}Added {args.url}")
    else:
        print(f"{ERROR}URL was already present.")

    with open(f"{working_dir}/archives.json", "w", encoding="utf-8") as f:
        json.dump(archives, f, ensure_ascii=False, indent=4)


elif args.command == "remove":
    archive_deleted = False
    for archive in archives["archives"]:
        if archive["url"] == args.url:
            archives["archives"].remove(archive)
            archive_deleted = True
            print(f"{INFO}Removed {args.url}")

    if not archive_deleted:
        print(f"{ERROR}Could not find the specified archive.")

    with open(f"{working_dir}/archives.json", "w", encoding="utf-8") as f:
        json.dump(archives, f, ensure_ascii=False, indent=4)

elif args.command == "sync":

    if args.url == "all":
        info_opts = {
            "ignoreerrors": True,
            # "dump_single_json": True,
            # "extract_flat": True,
            "add_metadata" "external_downloader": "aria2c",
            # TODO: Verify cookiefile functionality
            "cookiefile": f"{working_dir}/cookies.txt",
            "verbose": True,
            "download_archive": f"./.archive.txt",
            "external_downloader": "aria2c",
            "external_downloader_args": [
                "-c",
                "-x 16",
                "-s 16",
                "-j 16",
                "-k 1M",
                "--file-allocation=none",
            ],
        }

        for archive in archives["archives"]:
            with youtube_dl.YoutubeDL(info_opts) as ydl:
                info = ydl.extract_info(archive["url"], download=True)

        try:
            if not info:
                sys.exit("couldn't get info")
        except Exception:
            print(f"{WARN}Nothing to sync.")
            exit(1)

        with open(f"{working_dir}/out.json", "w", encoding="utf-8") as f:
            json.dump(info, f, ensure_ascii=False, indent=4)

        """
        [x] youtube-dl
        [x] --cookies cookies.txt
        [] --add-metadata
        [] --write-thumbnail
        [] --write-info-json
        [] --embed-subs
        [] --all-subs
        [x] --ignore-errors
        [] -f bestvideo+bestaudio
        [] -o "%(playlist_index)s-%(title)s.%(ext)s"
        [] "https://www.youtube.com/playlist?list=##########"
        [x] -v
        [x] --external-downloader aria2c
        [] --external-downloader-args "-x 16 -s 16 -j 16 --file-allocation=none"
        """
    else:
        print("elsed")


else:
    print("no")
